# System Security Baselines
## Firewall
### nftables on RHEL 8
In redhat 8.x we are going to use nftables instead of firewalld.

```
nft add table inet filter
nft add chain inet filter INPUT { type filter hook input priority 0 \; policy accept \;}
nft add chain inet filter FORWARD { type filter hook forward priority 0 \; policy accept \;}
nft add chain inet filter OUTPUT { type filter hook output priority 0 \; policy accept \;}
nft add rule inet filter INPUT iif lo accept
nft add rule inet filter INPUT ct state established,related accept
nft add rule inet filter INPUT tcp dport {22, 123} accept
nft add rule inet filter INPUT tcp dport {80, 443} accept
nft add rule inet filter INPUT counter drop
nft insert rule inet filter INPUT ip protocol icmp \  icmp type echo-request accept #Allow Ping
```
